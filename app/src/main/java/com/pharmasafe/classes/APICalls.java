package com.pharmasafe.classes;

/**
 * Created by wunmi on 12/05/2016.
 *
 *
 *
 */

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

//interface defining mthds for webcalls
public interface APICalls {

    @POST("?action=login")//&username={username}&password={password}
    //login
    Call<LoginResponse> login(@Query("username") String username, @Query("password") String password);

    //register
   // @FormUrlEncoded
    //@POST("http://mobilexcetera.com/pharmasafe/api/")
    //public void insetUser(@Field("name") String name, @Field("phone") String phone,
      //                              @Field("email") String email, @Field("dob") String dob,
        //                            @Field("username") String username, @Field("password") String password,
          //                Callback callback);
   // @FormUrlEncoded
  //  @POST("index.php")
  //  void insertUser(
            //@Field("name") String name, @Field("phone") String phone,
 //                                       @Field("email") String email, @Field("dob") String dob,
  //                                      @Field("username") String username, @Field("password") String password,
  //                                      Callback<LoginResponse> registerResponse
  //  );


//    //get deliveries/orders
//    @Headers({"Content-Type: application/json"})
//    @GET("https://e1.push.ng/v2/rider/orders")
//    Call<DeliveriesResponse> getOrders(@Header("Authorization") String authorization);


//    //get expenses
//    @Headers({"Content-Type: application/json"})
//    @GET("https://e1.push.ng/v2/rider/expense/list/{start}/{count}")
//    Call<ExpensesResponse> getExpenses(@Path("start") int start, @Path("count") int count, @Header("Authorization") String authorization);
//
//    //add expenses
//    @Headers({"Content-Type: application/json"})
//    @POST("https://e1.push.ng/v2/rider/expense/add")
//    Call<SimpleResponse> addExpense(@Body Parameters.ExpenseParameters expenseParameters, @Header("Authorization") String authorization);
//
//
//    //get expenses catgry
//    @Headers({"Content-Type: application/json"})
//    @GET("https://e1.push.ng/v2/rider/expense_category/list")
//    Call<ExpenseCategoryResponse> getExpenseCategory(@Header("Authorization") String authorization);
//
//    //getRiderStatus
//    @Headers({"Content-Type: application/json"})
//    @POST("https://e1.push.ng/v2/rider/status")
//    Call<SimpleResponse> getRiderStatus(@Header("Authorization") String authorization);
//
//    //setRiderStatus
//    @Headers({"Content-Type: application/json"})
//    @POST("https://e1.push.ng/v2/rider/{status}")
//    Call<SimpleResponse> setRiderStatus(@Path("status") String status, @Header("Authorization") String authorization);
//
//    //chng pwd
//    @Headers({"Content-Type: application/json"})
//    @POST("https://e1.push.ng/v2/rider/password/update")
//    Call<SimpleResponse> updatePassword(@Body Parameters.PasswordParameters passwordParameters, @Header("Authorization") String authorization);
//
//    //Send GPS Co-ordinates
//    @Headers({"Content-Type: application/json"})
//    @POST("https://e1.push.ng/v2/rider/mylocation")
//    Call<SimpleResponse> sendGPSCoordinates(@Body Parameters.LocationParameters locationParameters, @Header("Authorization") String authorization);
//
//    //Submit reqst Key
//    @Headers({"Content-Type: application/json"})
//    @POST("https://e1.push.ng/v2/rider/complete_split")
//    Call<SimpleResponse> submitRequest(@Body Parameters.SubmitRequestParameters submitRequestParameters, @Header("Authorization") String authorization);



}
