package com.pharmasafe.classes;



/**
 * Created by wunmi on 18/05/2016.
 */
public class LoginResponse{

    String code;
    Response response;

    public LoginResponse() {
    }

    public LoginResponse(String code, Response response) {
        this.code = code;
        this.response = response;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }


    class Response
    {
        String id;
        String name;
        String phone;
        String email;
        String dob;
        String username;
        String password;
        String registration_date;
        String extra;


        public Response() {
        }

        public Response( String id,String name, String phone, String email, String dob, String username, String password, String registration_date, String extra) {
            this.id = id;
            this.name = name;
            this.phone = phone;
            this.email = email;
            this.dob = dob;
            this.username = username;
            this.password = password;
            this.registration_date = registration_date;
            this.extra = extra;
        }

    }
    // this methods gets the name of the loggeIn user from the RESPONSE
    public String getName(){
        String name = getResponse().name;
        return name;
    }

}
