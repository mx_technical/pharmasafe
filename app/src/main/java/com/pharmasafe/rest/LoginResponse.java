package com.pharmasafe.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MYLO_PC on 22/09/2016.
 */

public class LoginResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("response")
    @Expose
    private LoginResponseData loginResponseData;

    /**
     *
     * @return
     * The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The response
     */
    public LoginResponseData getLoginResponseData() {
        return loginResponseData;
    }

    /**
     *
     * @param loginResponseData
     * The response
     */
    public void setLoginResponseData(LoginResponseData loginResponseData) {
        this.loginResponseData = loginResponseData;
    }

}
