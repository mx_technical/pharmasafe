package com.pharmasafe.rest;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Query;

public interface API_Service {

    @POST("?action=login")//&username={username}&password={password}
    Call<LoginResponse> loginUser(@Query("phone") String phone, @Query("password") String password);

    @FormUrlEncoded
    @POST("?action=register")
    Call<RegisterResponse> registerUser(@Field("name") String name, @Field("phone") String phone,
                                            @Field("email") String email, @Field("dob") String dob,
                                            @Field("username") String username, @Field("password") String password);
    @POST("?action=medication")
    Call<DrugSearchResponse> searchMedication(@Query("medication_name") String medication_name);
}
