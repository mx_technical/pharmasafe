package com.pharmasafe.rest;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class RestClient {
    private API_Service api_service;

    public RestClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Cons.BASE_URL_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api_service = retrofit.create(API_Service.class);
    }

    public API_Service getApi_service() {
        return api_service;
    }
}
