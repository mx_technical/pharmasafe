package com.pharmasafe.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MYLO_PC on 10/10/2016.
 */

public class DrugSearchResponseData {


    @SerializedName("medication_id")
    @Expose
    private String medicationId;
    @SerializedName("medication_name")
    @Expose
    private String medicationName;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("dosage")
    @Expose
    private String dosage;
    @SerializedName("drug_interactions")
    @Expose
    private String drugInteractions;
    @SerializedName("side_effects")
    @Expose
    private String sideEffects;
    @SerializedName("contra_indications")
    @Expose
    private String contraIndications;
    @SerializedName("patient_tips")
    @Expose
    private String patientTips;
    @SerializedName("other_info")
    @Expose
    private String otherInfo;
    @SerializedName("manufacturer_name")
    @Expose
    private String manufacturerName;



    /**
     *
     * @return
     * The medicationId
     */
    public String getMedicationId() {
        return medicationId;
    }

    /**
     *
     * @param medicationId
     * The medication_id
     */
    public void setMedicationId(String medicationId) {
        this.medicationId = medicationId;
    }

    /**
     *
     * @return
     * The medicationName
     */
    public String getMedicationName() {
        return medicationName;
    }

    /**
     *
     * @param medicationName
     * The medication_name
     */
    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    /**
     *
     * @return
     * The image
     */
    public Object getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(Object image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The dosage
     */
    public String getDosage() {
        return dosage;
    }

    /**
     *
     * @param dosage
     * The dosage
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    /**
     *
     * @return
     * The drugInteractions
     */
    public String getDrugInteractions() {
        return drugInteractions;
    }

    /**
     *
     * @param drugInteractions
     * The drug_interactions
     */
    public void setDrugInteractions(String drugInteractions) {
        this.drugInteractions = drugInteractions;
    }

    /**
     *
     * @return
     * The sideEffects
     */
    public String getSideEffects() {
        return sideEffects;
    }

    /**
     *
     * @param sideEffects
     * The side_effects
     */
    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    /**
     *
     * @return
     * The contraIndications
     */
    public String getContraIndications() {
        return contraIndications;
    }

    /**
     *
     * @param contraIndications
     * The contra_indications
     */
    public void setContraIndications(String contraIndications) {
        this.contraIndications = contraIndications;
    }

    /**
     *
     * @return
     * The patientTips
     */
    public String getPatientTips() {
        return patientTips;
    }

    /**
     *
     * @param patientTips
     * The patient_tips
     */
    public void setPatientTips(String patientTips) {
        this.patientTips = patientTips;
    }

    /**
     *
     * @return
     * The otherInfo
     */
    public String getOtherInfo() {
        return otherInfo;
    }

    /**
     *
     * @param otherInfo
     * The other_info
     */
    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    /**
     *
     * @return
     * The manufacturerName
     */
    public String getManufacturerName() {
        return manufacturerName;
    }

    /**
     *
     * @param manufacturerName
     * The manufacturer_name
     */
    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
}
