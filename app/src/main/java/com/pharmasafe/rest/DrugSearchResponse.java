package com.pharmasafe.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MYLO_PC on 10/10/2016.
 */

public class DrugSearchResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("response")
    @Expose
    private List<DrugSearchResponseData> searchResponseData = new ArrayList<DrugSearchResponseData>();

    /**
     *
     * @return
     * The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The response
     */
    public List<DrugSearchResponseData> getSearchResponseData() {
        return searchResponseData;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setSearchResponseData(List<DrugSearchResponseData> response) {
        this.searchResponseData = response;
    }
}
