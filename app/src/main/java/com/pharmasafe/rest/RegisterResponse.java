package com.pharmasafe.rest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MYLO_PC on 21/09/2016.
 */

public class RegisterResponse {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("response")
    @Expose
    private RegisterResponseData registerResponseData;

    /**
     *
     * @return
     * The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The response
     */
    public RegisterResponseData getRegisterResponseData () {
        return registerResponseData;
    }

    /**
     *
     * @param registerResponseData
     * The response
     */
    public void setRegisterResponseData(RegisterResponseData registerResponseData) {
        this.registerResponseData = registerResponseData;
    }
}
