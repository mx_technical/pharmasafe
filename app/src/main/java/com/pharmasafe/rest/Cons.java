package com.pharmasafe.rest;

//keeps constants
public class Cons {
    public static final String BASE_URL_ADDRESS ="http://mobilexcetera.com/pharmasafe/api/index.php";
    public static final String FOLDER_NAME ="PharmaSafe";
    public static final String ID ="ID";
    public static final String NAME ="NAME";
    public static final String PHONE ="PHONE";
    public static final String EMAIL ="EMAIL";
    public static final String DATE_OF_BIRTH="DATE_OF_BIRTH";
    public static final String USERNAME ="USERNAME";
    public static final String REGISTRATION_DATE ="REGISTRATION_DATE";
    public static final String DEFUALT = "N/A";
    public static final String PHONE_AS_LOGIN = "PHONE_AS_LOGIN";
}
