package com.pharmasafe.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pharmasafe.R;
import com.pharmasafe.rest.RegisterResponse;
import com.pharmasafe.rest.RestClient;


import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends AppCompatActivity {
    EditText f_Name;

    EditText s_Name;
    //EditText usernameEdit;
    EditText passwordEdit;

    EditText confirmPassword;
    EditText emailEdit ;

    EditText dobEdit;
    EditText phoneEdit;

    Button btn1 ;
    RelativeLayout relativeLayout;

    private DatePicker datePicker;
    private int _day, _month, _year;
    private Calendar calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        f_Name =(EditText)findViewById(R.id.f_name);
        s_Name = (EditText)findViewById(R.id.s_name);
      //  usernameEdit =(EditText)findViewById(R.id.username);
        passwordEdit = (EditText)findViewById(R.id.password);
        confirmPassword = (EditText)findViewById (R.id.confirmPassword);
        emailEdit =(EditText)findViewById(R.id.email);
        dobEdit = (EditText) findViewById(R.id.dob);
        phoneEdit =(EditText)findViewById(R.id.phone);
        btn1 = (Button)findViewById(R.id.regButton);
        relativeLayout =(RelativeLayout)findViewById(R.id.wrapper) ;



        calendar = Calendar.getInstance();


        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                calendar.set(Calendar.YEAR,year);
                calendar.set(Calendar.MONTH,month);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                _year = year;
                _month = month+1;
                _day = day;

                String s_year  = Integer.toString(_year);
                String s_month  = Integer.toString(_month);
                String s_day  = Integer.toString(_day);

              updateText(s_year,s_month,s_day);
            }
        };

        dobEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(RegisterActivity.this,dateSetListener,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s = emailEdit.getText().toString();
               if(f_Name.getText().toString().equals(" ") || f_Name.getText().toString().isEmpty()){
                   f_Name.setError("First Name is required");
               }
                else if(s_Name.getText().toString().equals(" ") || s_Name.getText().toString().isEmpty() )
                {
                    s_Name.setError("Other name is required");
                }

//                else if(usernameEdit.getText().toString().equals(" ") || usernameEdit.getText().toString().isEmpty()){
//                    usernameEdit.setError("Username is required");
//                }
                else if(emailEdit.getText().toString().equals(" " )|| emailEdit.getText().toString().isEmpty()){
                   emailEdit.setError("Email is required");
               }
                else if (!emailChecker(s)){
                    emailEdit.setError("Email is not valid");
                }
               else if(passwordEdit.getText().toString().equals(" " )|| passwordEdit.getText().toString().isEmpty()){
                   passwordEdit.setError("Email is required");
               }
               else if(confirmPassword.getText().toString().equals(" " )|| confirmPassword.getText().toString().isEmpty()){
                   confirmPassword.setError("Email is required");
               }
               else if(passwordEdit.getText().toString().length()<4|| confirmPassword.getText().toString().length()<4){
                   passwordEdit.setError("Email is required");
                   confirmPassword.setError("Email is required");
               }

               else if(!confirmPassword.getText().toString().equals(confirmPassword.getText().toString())){
                   Snackbar.make(relativeLayout,"Passwords do not match", Snackbar.LENGTH_LONG).show();
               }

                else {
                   insertUser();

               }

                //Toast.makeText(RegisterActivity.this, "INSERTING VALUES", Toast.LENGTH_SHORT).show();



            }

            private void insertUser() {
                final ProgressDialog mProgress = new ProgressDialog(RegisterActivity.this);
                mProgress.setTitle("Signing Up");
                mProgress.setMessage("Please wait...");
                mProgress.show();

                final AlertDialog mAlertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
                mAlertDialog.setTitle("SIGNING UP");
                mAlertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        if (mProgress.isShowing())
                            mProgress.dismiss();
                    }

                });
                final String name = f_Name.getText().toString() + " "+ s_Name.getText().toString();
                final String dateOfBirth = _year+"-"+ _month+"-"+ _day;
                final String u_name ="null by seun";
                Call<com.pharmasafe.rest.RegisterResponse> responseDataCall = new RestClient().getApi_service().registerUser(
                        name,
                        phoneEdit.getText().toString(),
                        emailEdit.getText().toString(),
                        dateOfBirth,
                        u_name,
                        passwordEdit.getText().toString()
                );
                responseDataCall.enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Response<RegisterResponse> response, Retrofit retrofit) {

                        //Log.v("RESPONSE: ", response.body().toString());
                        //Log.v("ResponseData", response.body().getRegisterResponseData().toString());
                        //Log.v("ResposneCode",response.body().getCode().toString());
                        //Log.v("ResponsedData1",response.body().getRegisterResponseData().getName());

                        if(response != null && response.body().getCode()==1){
                            mProgress.dismiss();
                            mAlertDialog.dismiss();
                            switch (response.body().getCode()){
                                case 1:

                                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    break;

                                case 0:

                                    mAlertDialog.setMessage("Wrong details");
                                    mAlertDialog.show();
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                       // Log.v(" throwable", t.toString());
                        mAlertDialog.setMessage("Signing up Failed");
                        mAlertDialog.show();
                    }
                });
            }
        });
    }

   private void updateText(String year, String month, String day) {
//        Toast.makeText(this, year+month+day, Toast.LENGTH_LONG).show();
            final String dob  = year + "-" + month + "-" + day;
            dobEdit.setText(dob);
    }

//    public boolean errorCheck(String test){
//
//       if(test.isEmpty()) return true;
//        return true;
//    }

    public boolean emailChecker(String email){
            Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
            boolean result = matcher.find();
        if (!result) {
            return false;
        } else {
            return true;
        }
    }
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                    Pattern.CASE_INSENSITIVE);
    //      /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/

//    public void password_check(EditText pwd1, EditText pwd2){
//        if(pwd1.getText().toString().isEmpty() || pwd1.getText().toString().equals("  ")){
//            pwd1.setError("Password required");
//        }
//
//        if(pwd2.getText().toString().isEmpty() || pwd2.getText().toString().equals("  ")){
//            pwd2.setError("Password required");
//        }
//    }

}


