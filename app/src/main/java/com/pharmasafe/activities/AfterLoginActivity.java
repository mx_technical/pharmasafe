package com.pharmasafe.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.pharmasafe.R;
import com.pharmasafe.fragments.DrugAuthenticationFragment;
import com.pharmasafe.fragments.DrugSearchFragment;
import com.pharmasafe.fragments.Drug_detailsFragment;
import com.pharmasafe.fragments.NewsFragment;
import com.pharmasafe.fragments.ProfileFragment;
import com.pharmasafe.fragments.User_historyFragment;
import com.pharmasafe.rest.Cons;

public class AfterLoginActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Context context;
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //the name the shows once you open the nav drawer
       // TextView drawer_username = (TextView)findViewById(R.id.drawer_user_name);
      //  String name= new LoginActivity().getResponse().getLoginResponseData().getName().toString();
       // if(name==null){
         //   drawer_username.setText("");
      //  }else{
        //    drawer_username.setText(name);
      //  }



        sharedPref =getSharedPreferences(Cons.FOLDER_NAME, MODE_PRIVATE);
        String u_name  =sharedPref.getString(Cons.NAME,Cons.DEFUALT);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        /**
         * i removed
         * app:headerLayout="@layout/nav_header_after_login"
         *
         * from the layout: activity_after_login.xml
         * under
         * <android.support.design.widget.NavigationView/>
         *
         * so as to inflate it programatically**/
        View hView = navigationView.inflateHeaderView(R.layout.nav_header_after_login);
        TextView drawer_username = (TextView) hView.findViewById(R.id.drawer_user_name);
        drawer_username.setText(u_name);



    }
/**
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LoginActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
               drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                View hView = navigationView.inflateHeaderView(R.layout.nav_header_after_login);
                TextView drawer_username = (TextView) hView.findViewById(R.id.drawer_user_name);
               // Bundle i = getIntent().getExtras();
                //if (i != null) {
                 //   String x = i.getString(LoginActivity.NAME);
                   // drawer_username.setText(x);
                //}
                Log.v("Intent data: ", data.toString());
                sharedPref =getSharedPreferences(Cons.FOLDER_NAME, context.MODE_PRIVATE);
                String u_name  =sharedPref.getString(Cons.NAME,Cons.DEFUALT);
                drawer_username.setText(u_name);
            }
        }
    } **/
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            }


    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.log_out)
        {
            Intent intent = new Intent(AfterLoginActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.drug_authentication)
        {
            Fragment fragment = new DrugAuthenticationFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                   // .add(R.id.content_after_login,fragment)
                    .replace(R.id.content_after_login,fragment)
                    //.addToBackStack("DrugAuthentication")
                    .commit();
            getSupportActionBar().setTitle(R.string.drug_authentication);
            //fragmentManager.popBackStack();



        }
        else if (id == R.id.drug_detail_form)
        {
            Fragment fragment = new Drug_detailsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
 //                   .add(R.id.content_after_login,fragment)
                    .replace(R.id.content_after_login,fragment)
                   // .addToBackStack("Drug_details")
                    .commit();
            getSupportActionBar().setTitle(R.string.drug_detail_form);
           // fragmentManager.popBackStack();

        }
        else if (id == R.id.user_history)
        {
            Fragment fragment = new User_historyFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
//                    .add(R.id.content_after_login,fragment)
                   .replace(R.id.content_after_login,fragment)
                    //.addToBackStack("User_history")
                    .commit();
            getSupportActionBar().setTitle(R.string.user_history);
            //fragmentManager.popBackStack();
        }
        else if (id == R.id.drug_info)
        {
            Fragment fragment = new DrugSearchFragment();
//                    Drug_InformationFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
//                    .add(R.id.content_after_login,fragment)
                    .replace(R.id.content_after_login,fragment)
                    //.addToBackStack("Drug_Information")
                    .commit();
            getSupportActionBar().setTitle(R.string.drug_information);
           // fragmentManager.popBackStack();
        }
        else if (id == R.id.profile)
        {
            Fragment fragment = new ProfileFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
//                    .add(R.id.content_after_login,fragment)
                    .replace(R.id.content_after_login,fragment)
                    //.addToBackStack("Profile")
                    .commit();
            getSupportActionBar().setTitle(R.string.profile);
         //  fragmentManager.popBackStack();

        }
        else if (id == R.id.news) {
            Fragment fragment = new NewsFragment();
            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
//                    .add(R.id.content_after_login,fragment)
                    .replace(R.id.content_after_login,fragment)
                   // .addToBackStack("News")
                    .commit();
            getSupportActionBar().setTitle(R.string.news);
           // fragmentManager.popBackStack();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
