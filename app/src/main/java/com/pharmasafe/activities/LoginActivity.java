package com.pharmasafe.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pharmasafe.R;
import com.pharmasafe.classes.APICalls;
import com.pharmasafe.classes.LoginResponse;
import com.pharmasafe.rest.Cons;
import com.pharmasafe.rest.RestClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private EditText usernameEditText;
    private EditText passwordEditText;
    Button loginButton;

    private View mProgressView;
    private View mLoginFormView;

    TextView signUpTextView;
    TextView forgotPwdTextView;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Context context;

    public final static String NAME="NAME";
    static final int REQUEST_CODE =1;

    public com.pharmasafe.rest.LoginResponse r;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameEditText = (EditText) findViewById(R.id.username);
        passwordEditText = (EditText) findViewById(R.id.password);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                attemptLogin();
            }
        });

        signUpTextView = (TextView) findViewById(R.id.signUp);
        forgotPwdTextView = (TextView) findViewById(R.id.forgotPassword);


        signUpTextView.setOnClickListener(this);
        forgotPwdTextView.setOnClickListener(this);

        sharedPref = getSharedPreferences(Cons.FOLDER_NAME, MODE_PRIVATE);
        String s = sharedPref.getString(Cons.PHONE_AS_LOGIN,Cons.DEFUALT);

        if(s!=null){
            usernameEditText.setText(s);
        }
        else if(s.equals("N/A")){
            usernameEditText.setText(" ");
        }
        else
        {
            usernameEditText.setText(" ");
        }


    }


    private void attemptLogin() {

        usernameEditText.setError(null);
        passwordEditText.setError(null);

        // Store values at the time of the login attempt.
        String u = usernameEditText.getText().toString();
        String username = "234"+u;
        String password = passwordEditText.getText().toString();

        //save the phone number against loggin in another time
        // in the format xx-xxxx-xxxx
        sharedPref = getSharedPreferences(Cons.FOLDER_NAME, MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.putString(Cons.PHONE_AS_LOGIN,u);
        editor.commit();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError("Invalid password! Empty password field.");
            focusView = passwordEditText;
            cancel = true;
        }

        // Check for a valid mobile
        if (TextUtils.isEmpty(username)) {
            usernameEditText.setError("Invalid username! Empty mobile field.");
            focusView = usernameEditText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a task to
            // perform the user login attempt.
            showProgress(true);


            Call<com.pharmasafe.rest.LoginResponse> loginResponseCall = new RestClient().getApi_service().loginUser(
                    username, password
            );
            loginResponseCall.enqueue(new Callback<com.pharmasafe.rest.LoginResponse>() {
                @Override
                public void onResponse(Response<com.pharmasafe.rest.LoginResponse> response, Retrofit retrofit) {
                    com.pharmasafe.rest.LoginResponse loginResponse = response.body();
                    r=response.body();
                    boolean loggedIn = false;
                    //saved the login details in the Database
                    saveIntoDatabase();

                    System.out.println("loginResponse-"+loginResponse.getCode());


                    if (loginResponse != null && loginResponse.getCode()==1)
                    {
                        //save to db
                        //loggedIn = Util.saveLoginResponse(this, response.body());
                        loggedIn = true;

                        if (loggedIn) {
                            Intent nextActivityIntent = new Intent(LoginActivity.this, AfterLoginActivity.class);
                            nextActivityIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                            //nextActivityIntent.putExtra(NAME,name);
                            //startActivityForResult(nextActivityIntent,REQUEST_CODE);
                            startActivity(nextActivityIntent);
                            finish();
                        }
                    }
                    else
                    {
                        passwordEditText.setError("Login failed! Please enter valid credentials.");
                        passwordEditText.requestFocus();

                    }


                    showProgress(false);

                }

                @Override
                public void onFailure(Throwable t) {
                    showProgress(false);

                    usernameEditText.setError("Login failed! Please check your connection.");
                    loginButton.requestFocus();
                }
            });
        }
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {

        final AlertDialog mAlertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        mAlertDialog.setTitle("Quit");
        mAlertDialog.setMessage("Are you sure you want to quit PharmaSafe");
        mAlertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }

        });
        mAlertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
            }
        });
        mAlertDialog.show();

    }

    //    private boolean isPasswordValid(String password) {
//        //TODO: Replace this with your own logic
//        return password.length() > 4;
//    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {

        int viewId = v.getId();

        Intent intent;

        switch (viewId) {

            case R.id.signUp:

                intent = new Intent(this, RegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                startActivity(intent);

                break;
        }
    }
    // public method for other activities to use LoginResponse
    //public com.pharmasafe.rest.LoginResponse getResponse(){
      //  return r;
    // }


    // saved all the response from Login into SharedPreferences
    public void saveIntoDatabase(){
        com.pharmasafe.rest.LoginResponseData response=r.getLoginResponseData();
        String id = response.getId();
        String name = response.getName().toString();
        String phone = response.getPhone();
        String email = response.getEmail();
        String dob = response.getDob();
        String username =response.getUsername();
        String reg_date =response.getRegistrationDate();


        //Log.v("Name: ", name);
        sharedPref=getSharedPreferences(Cons.FOLDER_NAME,MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.putString(Cons.ID,id);
        editor.putString(Cons.NAME,name);
        editor.putString(Cons.PHONE,phone);
        editor.putString(Cons.EMAIL,email);
        editor.putString(Cons.DATE_OF_BIRTH,dob);
        editor.putString(Cons.USERNAME,username);
        editor.putString(Cons.REGISTRATION_DATE,reg_date);


        editor.commit();

    }

}

