package com.pharmasafe.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.pharmasafe.R;

/**
 * Created by wunmi on 28/06/2016.
 */
public class LherbeurButtonRed extends Button {

    public LherbeurButtonRed(Context context) {
        super(context);
        setMyFont(context);
    }

    public LherbeurButtonRed(Context context, AttributeSet attrs) {
        super(context, attrs);
        setMyFont(context);
    }

    public LherbeurButtonRed(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setMyFont(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LherbeurButtonRed(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setMyFont(context);
    }



    public void setMyFont(Context context)
    {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "HelveticaNeue.ttf");
        setTypeface(typeface);

        //color
        setTextColor(getResources().getColor(android.R.color.white));
        setHintTextColor(getResources().getColor(android.R.color.white));

        //set bg
        setBackgroundResource(R.drawable.red_rounded_edge);
    }


}
