package com.pharmasafe;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pharmasafe.rest.DrugSearchResponseData;

import java.util.List;


public class RAdapter extends RecyclerView.Adapter<RAdapter.MyViewHolder> {

    private List<DrugSearchResponseData> drugSearchResponseDataList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
        }
    }


    public RAdapter(List<DrugSearchResponseData> drugSearchResponseDataList) {
        this.drugSearchResponseDataList = drugSearchResponseDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DrugSearchResponseData
                responseData = drugSearchResponseDataList.get(position);
//       for (i =0; i<=drugSearchResponseDataList.size(); i++){}
        holder.title.setText(responseData.getMedicationName());
        holder.genre.setText(responseData.getManufacturerName());
    }

    @Override
    public int getItemCount() {
        return drugSearchResponseDataList.size();
    }

}
