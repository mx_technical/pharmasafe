package com.pharmasafe.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pharmasafe.R;
import com.pharmasafe.activities.AfterLoginActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Drug_InformationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Drug_InformationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public Drug_InformationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Drug_InformationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Drug_InformationFragment newInstance(String param1, String param2) {
        Drug_InformationFragment fragment = new Drug_InformationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
       // ((AfterLoginActivity)getActivity()).setTitle(R.string.drug_information);
        //((AfterLoginActivity)getActivity()).getSupportActionBar().setTitle(R.string.drug_information);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drug__information_fragement, container, false);
    }


}
