package com.pharmasafe.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pharmasafe.R;
import com.pharmasafe.activities.AfterLoginActivity;
import com.pharmasafe.activities.LoginActivity;
import com.pharmasafe.rest.Cons;

public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    EditText username,phone,email,dob;
    TextView name;
    Button sign_out;

    SharedPreferences sharedPrefs ;


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
      //  ((AfterLoginActivity)getActivity()).setTitle(R.string.profile);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        AfterLoginActivity af = (AfterLoginActivity)getActivity();
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        name = (TextView)view.findViewById(R.id.profile_name);
        username = (EditText)view.findViewById(R.id.profile_username);
        phone = (EditText)view.findViewById(R.id.profile_phone_number);
        email = (EditText)view.findViewById(R.id.profile_email);
        dob = (EditText)view.findViewById(R.id.profile_dob);
        sign_out = (Button)view.findViewById(R.id.profile_sign_out);

        sharedPrefs = this.getActivity().getSharedPreferences(Cons.FOLDER_NAME, Context.MODE_PRIVATE);
        String s_name = sharedPrefs.getString(Cons.NAME,Cons.DEFUALT);
        String s_username = sharedPrefs.getString(Cons.USERNAME,Cons.DEFUALT);
        String s_phone = sharedPrefs.getString(Cons.PHONE,Cons.DEFUALT);
        String s_email = sharedPrefs.getString(Cons.EMAIL,Cons.DEFUALT);
        String s_dob = sharedPrefs.getString(Cons.DATE_OF_BIRTH,Cons.DEFUALT);

        name.setText("Hi, "+s_name);
        username.setText(s_username);
        phone.setText("+"+s_phone);
        email.setText(s_email);
        dob.setText(s_dob);

        sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });
        return view;
    }


}
