package com.pharmasafe.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.pharmasafe.R;
import com.pharmasafe.RAdapter;
import com.pharmasafe.activities.AfterLoginActivity;
import com.pharmasafe.rest.DrugSearchResponse;
import com.pharmasafe.rest.DrugSearchResponseData;
import com.pharmasafe.rest.RestClient;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DrugSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DrugSearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    final List<DrugSearchResponseData> drugSearchResponseDataList = new ArrayList<>();
    DrugSearchResponse resp ;


    public DrugSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DrugSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DrugSearchFragment newInstance(String param1, String param2) {
        DrugSearchFragment fragment = new DrugSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_drug_search, container, false);


        final RecyclerView recyclerView;
        final RAdapter mAdapter;
        final EditText searchParameter;
        final Button searchButton;
        final RelativeLayout relativeLayout;
        Context context = (((AfterLoginActivity)getActivity()).getApplicationContext());

        recyclerView = (RecyclerView)view.findViewById(R.id.rv);
        searchParameter = (EditText)view.findViewById(R.id.edit_drug_id);
        searchButton = (Button)view.findViewById(R.id.search);
        relativeLayout = (RelativeLayout)view.findViewById(R.id.drug_layout1);



        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //set the arrayList to_empty
//                searchParameter.setFocusable(false);
                recyclerView.setFocusable(true);
                if(!drugSearchResponseDataList.isEmpty()){
                    drugSearchResponseDataList.clear();
                }

                Snackbar.make(searchParameter,"Clicked", Snackbar.LENGTH_LONG).show();
                String param = searchParameter.getText().toString();
                Log.v("String: ", param);
                Call<DrugSearchResponse> searchCall = new RestClient().getApi_service().searchMedication(param);
                searchCall.enqueue(new Callback<DrugSearchResponse>() {
                    @Override
                    public void onResponse(Response<DrugSearchResponse> response, Retrofit retrofit) {
                        if(response.body().getCode()==0){
                            Snackbar.make(searchParameter,"No result", Snackbar.LENGTH_LONG).show();
                        }
                        else {
                            resp = response.body();
                            List<DrugSearchResponseData> ar = resp.getSearchResponseData();
                            if(!ar.isEmpty()){

                                Log.v("SearchResponse: ", "not empty");
                                ArrayList<DrugSearchResponseData> arrayList = new ArrayList(ar);
                                for(int i=0; i<arrayList.size(); i++){


                                   // Log.v("Medication:", arrayList.get(i).getMedicationName());
                                    DrugSearchResponseData responseData = new DrugSearchResponseData();
                                    responseData.setMedicationName(arrayList.get(i).getMedicationName());
                                    responseData.setManufacturerName(arrayList.get(i).getManufacturerName());
                                    drugSearchResponseDataList.add(responseData);
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.v("Error from search : " , t.toString());
                        Snackbar.make(searchParameter,"No result", Snackbar.LENGTH_LONG).show();
                    }
                });
            }


        });
        InputMethodManager inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(relativeLayout.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        mAdapter= new RAdapter(drugSearchResponseDataList);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setVisibility(View.VISIBLE);


        return view;
    }





}
